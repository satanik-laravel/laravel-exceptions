<?php

namespace Satanik\Exceptions\Types;

use Symfony\Component\HttpFoundation\JsonResponse;

class ExceptionRenderer
{
    public function render(Exception $e): ?JsonResponse
    {
        return $e->render();
    }
}
