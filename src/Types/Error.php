<?php

namespace Satanik\Exceptions\Types;

use Satanik\Exceptions\Concerns\ErrorNameRetrievable;
use Satanik\Exceptions\Contracts\ExposesErrors;
use Satanik\IoC\Concerns\Resolveable;
use Satanik\IoC\Contracts\Resolveable as ResolveableContract;

class Error implements ExposesErrors, ResolveableContract
{
    use Resolveable, ErrorNameRetrievable;
}
