<?php

namespace Satanik\Exceptions;

use Assert;
use Illuminate\Console\DetectsApplicationNamespace;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Satanik\Exceptions\Contracts\Assert as AssertContract;
use Satanik\Exceptions\Contracts\ExposesErrors;
use Satanik\Exceptions\Types\Assert as AssertClass;
use Satanik\Exceptions\Types\Error;

class ExceptionsServiceProvider extends ServiceProvider
{
    use DetectsApplicationNamespace;

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'satanik');

        $this->mergeTranslation('satanik', __DIR__ . '/../resources/lang');

        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/satanik-exceptions.php', 'satanik-exceptions');

        // TODO:
//        $errors = ClassLoader::inherits(Error::class)->filter(function ($error) {
//            try {
//                Assert::implements($error, ExposesFoundationErrors::class);
//                return starts_with($error, $this->getAppNamespace());
//            } catch (Exception $e) {
//                return false;
//            }
//        });
//        if ($errors->count() === 1) {
//            $this->app->bind(Error::class, $errors->first());
//        }

        $error = config('satanik-exceptions.class', Error::class);

        $this->app->bind(Error::class, function (Application $app, array $parameters) use ($error) {
            Assert::classExists($error);
            Assert::implements($error, ExposesErrors::class);

            if ($parameters['class'] ?? false) {
                return $error;
            }
            return new $error;
        });
        $this->app->alias(Error::class, 'satanik-error');

        $this->app->bind(AssertContract::class, AssertClass::class);
        $this->app->alias(AssertContract::class, 'satanik-assert');
    }

    private function mergeTranslation($key, $path): void
    {
        /** @var \Illuminate\Translation\Translator $translator */
        $translator = $this->app['translator'];
        /** @var \Satanik\Foundation\Classes\FileLoader $loader */
        $loader = $translator->getLoader();
        $loader->mergeOnce()->addNamespace($key, $path);
    }

    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole(): void
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__ . '/../config/satanik-exceptions.php' => config_path('satanik-exceptions.php'),
        ], 'satanik.exceptions.config');

        // Publishing the translation files.
        $this->publishes([
            __DIR__.'/../resources/lang' => resource_path('lang/vendor/satanik'),
        ], 'satanik.exceptions.translations');
    }
}
