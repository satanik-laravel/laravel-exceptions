<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 15.03.18
 * Time: 10:42
 */

namespace Satanik\Exceptions\Types;

use Illuminate\Http\JsonResponse;

class Redirect extends Exception
{
    protected $url;

    public function __construct(string $url = '', int $code = 302)
    {
        $this->url  = $url;
        $this->code = $code;
    }

    /**
     * @return JsonResponse
     */
    public function render(): JsonResponse
    {
        return response()
            ->json(
                null,
                $this->code,
                ['Location' => $this->url],
                JSON_UNESCAPED_SLASHES
            );
    }
}
