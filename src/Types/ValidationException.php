<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 01.02.18
 * Time: 02:40
 */

namespace Satanik\Exceptions\Types;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\JsonResponse;

class ValidationException extends Exception
{
    public function __construct(Validator $validator)
    {
        parent::__construct(
            trans('satanik::errors.validation_failed'),
            $validator,
            Error::VALIDATION_FAILED);
    }

    public function render(): JsonResponse
    {
        /** @var Validator $validator */
        $validator  = $this->data;
        $this->data = null;

        $errors         = $validator->getMessageBag();
        $errors         = $errors->toArray();
        $errors         = array_values($errors);
        $errors         = array_flatten($errors);
        $this->messages = $errors;

        return parent::render();
    }
}
