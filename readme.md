# Satanik Exceptions Package

[![Software License][ico-license]](LICENSE.md)

This package adds the macroable `Assert` facade to allow easier exception handling and standardised error codes.

### Dependencies

* satanik/ioc - [git@gitlab.com:satanik/laravel-ioc.git]()

## Structure

The directory structure follows the industry standard.

```bash
config/
src/
tests/
```


## Install

Via Composer

``` bash
$ composer require satanik/exceptions
```

if the package is not published use this in the composer file

```json
"repositories": {
  "satanik/exceptions": {
    "type": "vcs",
    "url": "git@gitlab.com:satanik/laravel-exceptions.git"
  },
  ...
},
...
"require": {
  ...
  "satanik/exceptions": "<version constraint>",
```

or copy the repository to

```bash
<project root>/packages/Satanik/Exceptions
```

and use this code in the composer file

```json
"repositories": {
  "satanik/exceptions": {
    "type": "path",
    "url": "packages/Satanik/Exceptions",
    "options": {
      "symlink": true
    }
  },
  ...
},
...
"require": {
  ...
  "satanik/exceptions": "@dev",
```

afterwards publish the configuration

```bash
$ php artisan vendor:publish --tag=satanik.exceptions.config
```

and if needed publish the translation files

```bash
$ php artisan vendor:publish --tag=satanik.exceptions.translations
```

## Usage

### Errors

This package manages a global list of errors that are mainly utilised by the `Assert` facade. Still, this list of errors is extendable by following the given steps.

First it is necessary to define an `Error` class and either let it use specific interfaces and traits or simply inherit from `Satanik\Exceptions\Types\Error`

```php
namespace App\Exceptions;

use Satanik\Exceptions\Concerns\ErrorNameRetrievable;
use Satanik\Exceptions\Contracts\ExposesErrors;
use Satanik\IoC\Concerns\Resolveable;
use Satanik\IoC\Contracts\Resolveable as ResolveableContract;

class Error implements ExposesErrors, ResolveableContract
{
    use Resolveable, ErrorNameRetrievable;
    
    public const CUSTOM_ERROR = 99999; // any number
}
```

or for simplicity

```php
namespace App\Exceptions;

use Satanik\Exceptions\Types\Error as BaseError;

class Error extends BaseError
{
    public const CUSTOM_ERROR = 99999; // any number
}
```

Next this class has to be exchanged in the config file `satanik-exceptions.php` published before.

```php
use App\Exceptions\Error;

return [
    'class' => Error::class,
];
```

The last step is to extend the translation files published to `resources/lang/vendor/satanik`.

**Note:** If other packages are added of the namespace `Satanik` they probably also contain error interfaces like `ExposesErrors`. All of them need to be added to the new `App\Exceptions\Error` class.

### Assert

`Assert` is a helper facade, providing many different checks. It is also utilised by other packages to broaden and simplify the process of throwing exceptions with predefined messages and error codes.

The class is `Macroable` and thus can be extended freely.

```php
class CustomServiceProvider
{
    public function boot()
    {
        /**
         * @param mixed $any
         * @param mixed $more
         */
        \Assert::macro('customAssert', function($any, ...$more): void {
            // do some checks with $any or $more
            \Assert::exception($result, \App\Exceptions\Error::CUSTOM_ERROR);
        });
    }
}
```

## Testing

``` bash
$ composer test
```

## Security

If you discover any security related issues, please email daniel@satanik.at instead of using the issue tracker.

## Credits

- [Daniel Satanik][link-author]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square

[link-author]: https://satanik.at
