<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 22.12.17
 * Time: 10:50
 */

return [
    'wrong_type' => 'the variables type is [:type_is] and should be [:type_should]',
    'not_existing' => 'the :type [:type_name] does not exist',
    'not_existing_in' => 'the :type [:type_name] does not exist in :type_container [:type_container_name]',
    'attribute_not_valid' => ':attribute [:value] is not valid if :attribute2 is :value2'
];
