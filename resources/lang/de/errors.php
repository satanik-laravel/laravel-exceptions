<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 09.11.17
 * Time: 15:21
 */

return [
    'unspecified'                     => ':message',
    'table_does_not_exist'            => 'Tabelle [:table] existiert nicht',
    'user_not_authorized'             => 'Der User mit der E-Mail [:user] ist nicht authorisiert diese Route zu nutzen',
    'table_not_deducable'             => 'Die Tabelle ist nicht ableitbar mit den vorhandenen Informationen',
    'method_not_deducable'            => 'Die Methode [:method] der Klasse [:class] ist nicht ableitbar mit den vorhandenen Informationen',
    'validation_failed'               => 'Die Validierung der Anfrage schlug fehl',
    'user_not_authorized_for_objects' => 'Der User mit der E-Mail [:user] ist nicht authorisiert [:type] mit den ids [:ids] zu manipulieren',
    'logic_flawed'                    => 'Es existiert ein Logikfehler im Backend: :message',
    'verification_failed'             => 'Die E-Mail [:email] konnte nicht verifiziert werden',
    'shell_command_failed'            => 'Shell Command schlug mit dem Code [:exit_code] fehl',
    'lat_lon_not_valid'               => 'Der Breitengrad [:latitude] und Längengrad [:longitude] sind nicht gültig',
    'api_query_over_limit'            => 'the api [:api] limit was exceeded',
    'api_request_denied'              => 'the api [:api] request was denied',
    'api_invalid_request'             => 'the api [:api] request was invalid',
    'api_unknown_error'               => 'the api [:api] yields an unknown error, it may succeed on retry',
    'api_invalid_result'              => 'the api [:api] yields an invalid result',
    'address_not_valid'               => 'Wir können die Adresse [:address] leider nicht finden',
    'verification_code_expired'       => 'Der Verifizierungs-Code für die E-mail [:email] ist abgelaufen',
    'not_enough_credits'              => 'Der User mit der E-Mail [:email] hat nicht genug Credits [Aktuell: :credits] um [:items] zu kaufen, welche [:costs] Credits kosten',
    'class_not_deducable'             => 'Klasse [:class] ist nicht ableitbar mit den vorhandenen Informationen',
    'class_does_not_exist'            => 'Die Klasse [:class] existiert nicht',
    'class_misses_trait'              => 'Die Klasse [:class] vermisst und muss den Trait [:trait] nutzen',
    'class_misses_interface'          => 'Die Klasse [:class] vermisst und muss das Interface [:interface] implementieren',
    'var_is_not_a'                    => 'Die Variable [:variable] sollte den Typ [:type] haben',
    'class_does_not_inherit'          => 'Die Klasse [:class] muss von der Klasse [:parent] erben',
    'login_failed'                    => 'Das Passwort scheint nicht korrekt zu sein',
    'already_friends'                 => 'Du bist schon mit :firstname :lastname (:username) befreundet',
    'not_set'                         => 'Das Element [:name] ist nicht gesetzt worden',
    'not_found'                       => 'Die Resource [:resource] kann nicht gefunden werden',
    'method_not_allowed'              => 'Diese Resource ist nur über [:allowed] erreichbar, nicht über [:current]',
];
