<?php

namespace Satanik\Exceptions\Types;

use Illuminate\Support\Str;
use Illuminate\Support\Traits\Macroable;
use Satanik\Exceptions\Contracts\Assert as AssertContract;
use Validator;

class Assert implements AssertContract
{
    use Macroable;

    /**
     * @param bool  $condition
     * @param int   $error
     * @param array $data
     *
     * @throws \Satanik\Exceptions\Types\Exception
     */
    public function exception(bool $condition, int $error, array $data = [], int $code = 400): void
    {
        if (!$condition) {
            try {
                $r         = new \ReflectionClass(Error::bound());
                $constants = collect($r->getConstants());
                $constant  = $constants->filter(function ($value) use ($error) {
                    return $value === $error;
                });
                $key       = $constant->keys()->first();
                $error     = $constant->first();
                throw new Exception($error, $data, 1, $code);
            } catch (\ReflectionException $e) {
                \Log::error($e->getMessage());
            }
        }
    }

    /**
     * @param mixed $var
     *
     * @throws \Satanik\Exceptions\Types\Exception
     */
    public function isset($var): void
    {
        $this->exception(isset($var), Error::NOT_SET);
    }

    /**
     * @param array $data
     * @param array $rules
     * @param array $messages
     * @param array $customAttributes
     *
     * @static
     * @throws \Satanik\Exceptions\Types\ValidationException
     */
    public function validation(array $data, array $rules, array $messages = [], array $customAttributes = []): void
    {
        $v = Validator::make($data, $rules, $messages, $customAttributes);
        if ($v->fails()) {
            throw new ValidationException($v);
        }
    }

    /**
     * @param string $class
     *
     * @throws \Satanik\Exceptions\Types\Exception
     */
    public function classExists(string $class): void
    {
        $this->exception(class_exists($class), Error::CLASS_DOES_NOT_EXIST, [
            'class' => $class,
        ]);
    }

    /**
     * @param string             $class
     * @param mixed|array|string $interfaces
     *
     * @throws \Satanik\Exceptions\Types\Exception
     * @throws \Satanik\Exceptions\Types\ValidationException
     */
    public function implements(string $class, $interfaces): void
    {
        $interfaces = is_array($interfaces) ? $interfaces : [$interfaces];
        foreach ($interfaces as $interface) {
            $this->validation(['interface' => $interface], ['interface' => 'required|string']);

            $this->exception(
                \in_array($interface, \class_implements($class), true),
                Error::CLASS_MISSES_INTERFACE,
                [
                    'class'     => $class,
                    'interface' => $interface,
                ]);
        }
    }

    /**
     * @param string $class
     * @param string $parent
     *
     * @throws \Satanik\Exceptions\Types\Exception
     */
    public function inherits(string $class, string $parent): void
    {
        $this->exception(
            \is_subclass_of($class, $parent),
            Error::CLASS_DOES_NOT_INHERIT,
            [
                'class'  => $class,
                'parent' => $parent,
            ]
        );
    }

    /**
     * @param string             $class
     * @param mixed|array|string $traits
     *
     * @throws \Satanik\Exceptions\Types\Exception
     * @throws \Satanik\Exceptions\Types\ValidationException
     */
    public function uses($class, $traits): void
    {
        $class  = is_object($class) ? get_class($class) : $class;
        $traits = is_array($traits) ? $traits : [$traits];
        foreach ($traits as $trait) {
            $this->is($trait, 'string', '$trait');
            $this->validation(['trait' => $trait], ['trait' => 'required|string']);

            $this->exception(
                \in_array($trait, \class_uses_deep($class, true), true),
                Error::CLASS_MISSES_TRAIT,
                [
                    'class' => $class,
                    'trait' => $trait,
                ]);
        }
    }

    /**
     * @param mixed       $variable
     * @param string      $type
     * @param string|null $name
     *
     * @throws \Satanik\Exceptions\Types\Exception
     */
    public function is($variable, string $type, string $name = null): void
    {
        $method = "is_$type";
        $this->exception(
            function_exists($method),
            Error::METHOD_NOT_DEDUCABLE,
            [
                'method' => $method,
                'class'  => 'global',
            ]
        );

        $this->exception(
            $method($variable),
            Error::VAR_IS_NOT_A,
            [
                'variable' => $name ?? '[unknown]',
                'type'     => $type,
            ]
        );
    }

    /**
     * @param bool        $condition
     * @param string|null $message
     *
     * @throws \Satanik\Exceptions\Types\Exception
     */
    public function flawlessLogic(bool $condition, string $message = null): void
    {
        $this->exception($condition, Error::LOGIC_FLAWED, ['message' => $message]);
    }
}
