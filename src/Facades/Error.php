<?php

namespace Satanik\Exceptions\Facades;

use Illuminate\Support\Facades\Facade;

class Error extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'satanik-error';
    }
}
