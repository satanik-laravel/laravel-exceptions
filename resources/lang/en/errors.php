<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 09.11.17
 * Time: 15:21
 */

return [
    'unspecified'                     => ':message',
    'table_does_not_exist'            => 'table [:table] does not exist',
    'user_not_authorized'             => 'user with email [:user] is not authorized to use this route',
    'table_not_deducable'             => 'the table is not deducable from the given information',
    'method_not_deducable'            => 'method [:method] of the class [:class] is not deducable from the given information',
    'validation_failed'               => 'validation of request failed',
    'user_not_authorized_for_objects' => 'user with email [:user] is not authorized to manipulate [:type] with the ids [:ids]',
    'logic_flawed'                    => 'there is a logic flaw in the backend: :message',
    'verification_failed'             => 'the email [:email] could not be verified',
    'shell_command_failed'            => 'shell command failed with exit code [:exit_code]',
    'lat_lon_not_valid'               => 'the latitude [:latitude] and longitude [:longitude] are not yielding suitable results',
    'api_query_over_limit'            => 'the api [:api] limit was exceeded',
    'api_request_denied'              => 'the api [:api] request was denied',
    'api_invalid_request'             => 'the api [:api] request was invalid',
    'api_unknown_error'               => 'the api [:api] yields an unknown error, it may succeed on retry',
    'api_invalid_result'              => 'the api [:api] yields an invalid result',
    'address_not_valid'               => 'unfortunately, we cannot find the address [:address]',
    'verification_code_expired'       => 'the verification code for [:email] expired',
    'not_enough_credits'              => 'the user with the email [:email] has not enough credits [currently: :credits] to buy [:items] which cost [:costs]',
    'class_not_deducable'             => 'class [:class] is not deducable from the given information',
    'class_does_not_exist'            => 'The class [:class] does not exist',
    'class_misses_trait'              => 'The class [:class] misses and needs to use the trait/concern [:trait]',
    'class_misses_interface'          => 'The class [:class] misses and needs to implement the interface [:interface]',
    'var_is_not_a'                    => 'The variable [:variable] is not of type [:type]',
    'class_does_not_inherit'          => 'The class [:class] needs to inherit from the class [:parent]',
    'login_failed'                    => 'The password seems to be wrong',
    'not_set'                         => 'The entity [:name] is not set',
    'not_found'                       => 'The resource [:resource] is not found',
    'method_not_allowed'              => 'This resource is only available through [:allowed], not through [:current]',
];
