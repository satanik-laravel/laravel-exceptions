<?php

namespace Satanik\Exceptions\Concerns;

use ReflectionClass;

trait ErrorNameRetrievable
{
    /**
     * @param int $code
     *
     * @return null|string
     */
    public static function retrieve(int $code): ?string
    {
        try {
            $r = new ReflectionClass(new self());
            $constants = $r->getConstants();
            foreach ($constants as $name => $value) {
                if ($code === $value) {
                    return $name;
                }
            }
        } catch (\ReflectionException $e) {
        }
        return null;
    }
}
