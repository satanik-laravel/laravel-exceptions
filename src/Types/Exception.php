<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 01.10.17
 * Time: 22:44
 */

namespace Satanik\Exceptions\Types;

use Exception as BaseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Str;
use Lang;

/**
 * Class Exception
 * @package Satanik\Foundation\Exceptions
 *
 * @property-write int    code
 * @property-write int    line
 * @property-write string file
 * @property-write array  trace
 */
class Exception extends BaseException
{
    protected $data;
    protected $error;
    protected $messages;
    protected $trace;

    /**
     * Exception constructor.
     *
     * @param string|string[]  $messages
     * @param null|array|mixed $data
     * @param int              $error
     * @param int              $code
     */
    public function __construct($messages, $data = null, int $error = 1, int $code = 400)
    {
        $this->code     = $code;
        $this->messages = $messages;
        $this->data     = $data;
        $this->error    = $error;
    }

    public function __set($name, $value)
    {
        if (in_array($name, ['code', 'file', 'line', 'trace'])) {
            $this->$name = $value;
        }
    }

    /**
     * @return null|JsonResponse
     */
    public function render(): ?JsonResponse
    {
        $this->handleCodedMessages();
        return resolve(ExceptionRenderer::class)->render($this);
    }

    private function handleCodedMessages(): void
    {
        $error = $this->messages;
        if (\is_array($error) || !\is_int($error)) {
            return;
        }

        $name = Str::lower(Error::retrieve($error));
        if (!$name || !Lang::has("satanik::errors.$name")) {
            return;
        }

        $data = $this->data;

        if (\is_array($data)) {
            $message = trans("satanik::errors.$name");
            preg_match_all('/(?<=:)\w+/', $message, $matches, PREG_SET_ORDER, 0);
            $matches = array_flatten($matches);
            $params  = array_intersect_key($data, array_flip($matches));
        }

        if ($this->error !== 1) {
            $this->code = $this->error;
        }
        $this->error    = $error;
        $this->messages = trans("satanik::errors.$name", $params ?? []);

        if (\is_array($data) && $params !== null && \is_array($params)) {
            $this->data = array_diff_key($data, $params);
        }
    }

    /**
     * @return array|mixed|null
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return int
     */
    public function getError(): int
    {
        return $this->error;
    }

    /**
     * @return string|string[]
     */
    public function getMessages()
    {
        return $this->messages;
    }


}
