<?php

namespace Satanik\Exceptions\Concerns;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Satanik\Exceptions\Types\Error;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait RendersExceptions
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \Exception               $exception
     *
     * @return null|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function renderExceptions($request, Exception $exception): ?JsonResponse
    {
        if ($exception instanceof AuthenticationException) {
            $auth             = $exception;
            $exception        = new \Satanik\Exceptions\Types\Exception($auth->getMessage(), null, 1, 401);
            $exception->code  = 401;
            $exception->line  = $auth->getLine();
            $exception->file  = $auth->getFile();
            $exception->trace = $auth->getTrace();
        }

        if ($exception instanceof AuthorizationException) {
            $auth             = $exception;
            $exception        = new \Satanik\Exceptions\Types\Exception($auth->getMessage(), null, 1, 403);
            $exception->line  = $auth->getLine();
            $exception->file  = $auth->getFile();
            $exception->trace = $auth->getTrace();
        }

        if ($exception instanceof ModelNotFoundException || $exception instanceof NotFoundHttpException) {
            $found            = $exception;
            $exception        = new \Satanik\Exceptions\Types\Exception(Error::NOT_FOUND, [
                'resource' => $request->getPathInfo(),
            ], 1, 404);
            $exception->line  = $found->getLine();
            $exception->file  = $found->getFile();
            $exception->trace = $found->getTrace();
        }

        if ($exception instanceof MethodNotAllowedHttpException) {
            $allowed          = $exception;
            $exception        = new \Satanik\Exceptions\Types\Exception(Error::METHOD_NOT_ALLOWED, [
                'allowed' => $exception->getHeaders()['Allow'],
                'current' => $request->method(),
            ], 1, 405);
            $exception->line  = $allowed->getLine();
            $exception->file  = $allowed->getFile();
            $exception->trace = $allowed->getTrace();
        }

        if ($exception instanceof HttpException) {
            $http             = $exception;
            $exception        = new \Satanik\Exceptions\Types\Exception(Error::UNSPECIFIED, [
                'message' => $http->getMessage(),
            ],1, $http->getStatusCode());
            $exception->line  = $http->getLine();
            $exception->file  = $http->getFile();
            $exception->trace = $http->getTrace();
        }

        if ($exception instanceof \Satanik\Exceptions\Types\Exception) {
            return $exception->render();
        }

        return null;
    }
}
