<?php

namespace Satanik\Exceptions\Facades;

use Illuminate\Support\Facades\Facade;

class Assert extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     *
     * @throws \RuntimeException
     */
    protected static function getFacadeAccessor(): string
    {
        return 'satanik-assert';
    }

}
