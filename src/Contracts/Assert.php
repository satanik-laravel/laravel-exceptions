<?php

namespace Satanik\Exceptions\Contracts;

interface Assert
{
    /**
     * @param bool  $condition
     * @param int   $error
     * @param array $data
     */
    public function exception(bool $condition, int $error, array $data = []): void;

    /**
     * @param mixed $var
     */
    public function isset($var): void;

    /**
     * @param array $data
     * @param array $rules
     * @param array $messages
     * @param array $customAttributes
     *
     * @static
     */
    public function validation(array $data, array $rules, array $messages = [], array $customAttributes = []): void;

    /**
     * @param string $class
     */
    public function classExists(string $class): void;

    /**
     * @param string             $class
     * @param mixed|array|string $interfaces
     */
    public function implements(string $class, $interfaces): void;

    /**
     * @param string $class
     * @param string $parent
     */
    public function inherits(string $class, string $parent): void;

    /**
     * @param string             $class
     * @param mixed|array|string $traits
     */
    public function uses($class, $traits): void;

    /**
     * @param mixed       $variable
     * @param string      $type
     * @param string|null $name
     */
    public function is($variable, string $type, string $name = null): void;

    /**
     * @param bool        $condition
     * @param string|null $message
     */
    public function flawlessLogic(bool $condition, string $message = null): void;
}
