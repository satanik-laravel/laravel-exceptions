<?php

namespace Satanik\Exceptions\Contracts;

interface ExposesErrors
{
    public const UNSPECIFIED                     = 10000;
    public const TABLE_DOES_NOT_EXIST            = 10001;
    public const USER_NOT_AUTHORIZED             = 10002;
    public const TABLE_NOT_DEDUCABLE             = 10003;
    public const METHOD_NOT_DEDUCABLE            = 10004;
    public const VALIDATION_FAILED               = 10005;
    public const USER_NOT_AUTHORIZED_FOR_OBJECTS = 10006;
    public const LOGIC_FLAWED                    = 10007;
    public const VERIFICATION_FAILED             = 10008;
    public const SHELL_COMMAND_FAILED            = 10009;
    public const LAT_LON_NOT_VALID               = 10010;
    public const API_QUERY_OVER_LIMIT            = 10011;
    public const API_REQUEST_DENIED              = 10012;
    public const API_INVALID_REQUEST             = 10013;
    public const API_UNKNOWN_ERROR               = 10014;
    public const API_INVALID_RESULT              = 10015;
    public const ADDRESS_NOT_VALID               = 10016;
    public const VERIFICATION_CODE_EXPIRED       = 10017;
    public const NOT_ENOUGH_CREDITS              = 10018;
    public const CLASS_NOT_DEDUCABLE             = 10019;
    public const CLASS_DOES_NOT_EXIST            = 10020;
    public const CLASS_MISSES_TRAIT              = 10021;
    public const CLASS_MISSES_INTERFACE          = 10022;
    public const VAR_IS_NOT_A                    = 10023;
    public const CLASS_DOES_NOT_INHERIT          = 10024;
    public const LOGIN_FAILED                    = 10025;
    public const NOT_SET                         = 10026;
    public const NOT_FOUND                       = 10027;
    public const METHOD_NOT_ALLOWED              = 10028;

    /**
     * @param int $code
     *
     * @return null|string
     */
    public static function retrieve(int $code): ?string;
}
